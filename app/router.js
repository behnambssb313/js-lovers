
const authRouter = require('./modules/auth/routes');
const usersRouter = require('./modules/users/routes');
const {auth,isAdmin}=require('./modules/users/middlewars');


module.exports=(app)=>{
  app.use('/api/v1/auth',authRouter);
  app.use('/api/v2/users',[auth], usersRouter);
  
};


