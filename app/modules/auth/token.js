const jwt=require('jsonwebtoken');

exports.findtoken=(req)=>{
    if(!('authorization' in req.headers)){
        return false
    }
    const {authorization}=req.headers;
    const[prefix,token]=authorization.split(' ');
    if(!token){
        return false;

    }
    return token;
}

exports.genrator=(params)=>{
    return jwt.sign(params,process.env.JWT_SECRET);
};

exports.verify=(token)=>{
    try {
        const payload=jwt.verify(token,process.env.JWT_SECRET);
        return payload;   
    } catch (error) {
        return false;
        
    }
}