const connection=require('./../../../database/connection/connection');




exports.userLogin=async(email,password)=>{
    const db=await connection();
    const[result,fields]=await db.query('SELECT * FROM auth_users WHERE email=? AND password=? LIMIT 1',[email,password]);
    return result.affectedRows > 0;
}

exports.userRegister=async(userDataRegister)=>{
    const db=await connection();
    const[rows,fields]=await db.query('INSERT INTO auth_users SET ?',[userDataRegister]);
    return rows.Insertid;
}
