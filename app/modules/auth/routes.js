const express = require('express');
const authController = require('./controller');

const router = express.Router();


router.get('/',authController.index);
router.post('/login',authController.Login);
router.post('/register',authController.register);

module.exports = router;
