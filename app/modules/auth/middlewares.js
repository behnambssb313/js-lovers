const tokenservice=require('./token');

exports.auth=(req,res,next)=>{
    const token=tokenservice.findtoken(req);

    if(!token){
        res.status(401).send({
            success:false,
            message:"you dont have ..."
        })
    }

    const payload=tokenservice.verify(token);
    if(!payload){
        res.status(401).send({
            success:false,
            message:"you dont have payload werify ..."
        })
    }

    return next();

}



exports.isAdmin=(req,res,next)=>{
    const token=tokenservice.findtoken(req);
    const payload=tokenservice.verify(token);

    if(parseInt(payload.isAdmin) === 1){
        return next();
   }
   return res.status(403).send({
    success:false,
    message:"forbidden!"
});


}