exports.removePassword=(user)=>{
    delete user.password
    return user;
}

exports.fullName=(user)=>{
    user.full_name=`${user.frist_name} ${user.last_name}`;
    return user;
}

exports.removeAdminFlag=(user)=>{
    delete user.IS_Admin
    return user;
}