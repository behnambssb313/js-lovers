const userModel=require('./userModel');
const tokenservice=require('./token');
const prsenter=require('./presenter');



exports.showUsers=async(req,res)=>{

    let {page,number}=req.query;
    if(!page){
        page=1;
        number=10;
    }
    const users=await userModel.getUsers(page,number);
    const totalUsers=await userModel.countUsers(); // count
    const totalCount=totalUsers / number; // count_page
    const pageination={
        total:totalUsers,
        next_page:page < totalCount ? `http://localhost:3019/api/v2/users/list?page=${parseInt(page)+1}&number=${number}` : null,
        prev_page:page > 1 ? `http://localhost:3019/api/v2/users/list?page=${page-1}&number=${number}` : null,
        page,
        per_page:number
    };
    

    res.send({
        data:users.map(prsenter.removeAdminFlag).map(prsenter.removePassword).map(prsenter.fullName),
        meta:{
            pageination
        }
    });

};
exports.createUsers=async(req,res)=>{
    const newUsersData=req.body;
    const newUserid= await userModel.create(newUsersData);
    return res.status(201).send({
        success:true,
        message: 'کاربر جدید با موفقیت ایجاد شد',
        uid: newUserid    
    });
};
exports.deleteUsers=async(req,res)=>{
    const deleteUsersData=req.params.id;
    const deleteUserid= await userModel.delete(deleteUsersData);
    if (deleteUserid) {
        return res.send({
            success: true,
            message: 'کاربر مورد نظر با موفقیت حذف گردید',
        });
    }
    return res.send({
        success: false,
        message: 'خطایی در حذف کاربر رخ داده است',
    });
};



exports.updateGet=async(req,res)=>{
    const updateId=req.params.id;
    const updateUserid= await userModel.updateGet(updateId);
    console.log(updateUserid);
    if(updateUserid) {
        return res.send({
        success: true,
        message: 'کاربر موردنظر انتخاب و آماده ویرایش می باشد',
        updateUserid
    });
}
    return res.send({
        success: false,
        message: 'خطایی رخ داده و کاربر موردنظر انتخاب نشد'
        
    });
}
exports.updatePut=async(req,res)=>{
    const updateId=req.params.id;
    const updateData=req.body;
    const updateUserid= await userModel.updatePut(updateData,updateId);
    console.log(updateUserid);
    if(updateUserid) {
        return res.send({
        success: true,
        message: 'کاربر موردنظر انتخاب و آماده ویرایش می باشد',
        updateUserid
    });
}
    return res.send({
        success: false,
        message: 'خطایی رخ داده و کاربر موردنظر انتخاب نشد'
        
    });
}

exports.Login=async(req,res)=>{
    const {email,password}=req.body;
    if(!email || !password){
        res.status(422).send({
            success:false,
            message:"نام کاربری یا کلمه عبور اشتباه است"
        })
    }
    const getUserscerdentional=await userModel.getUserscerdentional(email,password);
    if(!getUserscerdentional){
        res.status(422).send({
            success:false,
            message:"کاربری با این مشخصات یافت نشد"
        })
    }

    const token=tokenservice.genrator({
        uid:getUserscerdentional.id,
        isAdmin:getUserscerdentional.IS_Admin
    })

    res.send({
        success:true,
        token
    })
    

}
exports.Register=async(req,res)=>{

    const {frist_name,last_name,email,password,phone}=req.body;
    
    if(!frist_name || !last_name || !email || !password || !phone){
        res.status(422).send({
            success:false,
            message:"نام کاربری یا کلمه عبور اشتباه است"
        })
    }
    const newUser=await userModel.userRegister(frist_name,last_name,email,password,phone);
    res.send({
        success:true,
        message:'دمت گرم کاربر رو ایجاد کردیم',
        newUser
    })
}