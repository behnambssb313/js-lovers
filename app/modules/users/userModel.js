const connection=require('../../../database/connection/connection')

exports.getUsers=async (page = 1, number = 10)=>{
    const db = await connection();
    const offset = (page - 1) * number;
    const [users, fields] = await db.query(`SELECT * FROM users LIMIT ${number} OFFSET ${offset}`);
    return users;
}


exports.countUsers=async()=>{
    const db=await connection();
    const[result,fields]=await db.query("SELECT COUNT(id) as total FROM users");
    return result[0].total;
}

exports.create=async(newUsersData)=>{
    const db=await connection();
    const[result,fields]=await db.query("INSERT INTO users SET ?",[newUsersData]);
    return result.insertId;
}

exports.delete=async(id)=>{
    const db=await connection();
    const[result,fields]=await db.query("DELETE FROM users WHERE id=? LIMIT 1",[id]);
    return result.affectedRows > 0;

}

exports.updateGet=async(id)=>{
    const db=await connection();
    const[result,fields]=await db.query("SELECT * FROM users WHERE id=? ",[id]);
    return result;
}
exports.updatePut=async(updateData,id)=>{
    const db=await connection();
    const[result,fields]=await db.query("UPDATE users SET ? WHERE id=?",[updateData,id]);
    return result;
}

exports.getUserscerdentional=async(email,password)=>{
    const db=await connection();
    const[rows,fields]=await db.query("SELECT * FROM users WHERE email=? AND password=? LIMIT 1",[email,password]);
    if(rows.length >0){
        return rows[0];
    }
    return false;
}
exports.userRegister=async (frist_name,last_name,email,password,phone)=>{
    const db=await connection();
    const [result,fields]=await db.query("INSERT INTO users SET ?",[frist_name,last_name,email,password,phone]);
    return result.insertId;
}

