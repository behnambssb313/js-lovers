const express = require('express');
const usersController=require('./controller');


const router = express.Router();

router.get('/list',usersController.showUsers);
router.post('/list',usersController.createUsers);
router.delete('/list/:id',usersController.deleteUsers);

router.get('/list/:id',usersController.updateGet);
router.put('/list/:id',usersController.updatePut);


router.post('/login',usersController.Login);
router.post('/register',usersController.Register);



module.exports=router;
