const express=require('express');
const app=express();

require('./middlewares/middleware')(app);
require('./router')(app);
require('./middlewares/404')(app);


const runApplication = () => {
    app.listen(process.env.APP_PORT, () => {
        console.log(`app is running on port ${process.env.APP_PORT}`);
    });
};

module.exports = runApplication;